(ns ursound.scratch)

;(loop []
;  (when (not (. System/in available))
;    (println "woo")
;    (Thread/sleep 1000)
;    (recur)))

(defn key?
  []
  true)

(defn key?
  []
  (. System/in available))

(defn again
  [x keypress]
  (and (> x 0) keypress))

(defn once
  [x keypress]
  (println x keypress)
  (Thread/sleep 1000))

(defn go ""
  [times]
  (loop [x times]
    (let [keypress (key?)]
      (when (again x keypress)
        (once x keypress)
        (recur (- x 1))))))

(go 10)

(if (key?) (println (read-line)))

