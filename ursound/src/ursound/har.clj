(ns ursound.har
  (:use clojure.test)
  (:use overtone.core)
  (:use ursound.core))

; generate a random squence
(defn up [c] (let [n+ (rand-int (count c))
                   v+ (inc (nth c n+))
                   go (< v+ 4)
                   ]
               (if go (assoc c n+ v+))
               ))

(defn level [c] (let [n+ (rand-int (count c))
                   n- (rand-int (count c))
                   v+ (inc (nth c n+))
                   v- (dec (nth c n-))
                   go (and (not= n+ n-) (< v+ 4) (> v- -4))
                   ]
               (if go (assoc c n+ v+ n- v-) c)
               ))

(defn rand-seq [n]
  "generates a random sequnece that with the same sum as the input vector"
  (let [first (up (up (vec (repeat n 0))))]
    (nth (iterate level first) (* n 2))))

; tests
(deftest rand-seq-sum-equals-2
  (let [actual (rand-seq 16)]
  (is (= 2 (apply + actual)))))


; play a note and a song
(defn fq-note
  "Play a single note at the given frequency"
  [instr t freq ms-length vol]
  (at (+ t (now)) (apply instr [freq (/ ms-length 1000.0) vol])))

(defn ms-beat
  "the number of milliseconds per beat"
  [bpm]
  (-> 1000 (* 60) (/ bpm)))

(defn plink
  "Play a single note"
  [instr t step ms-length vol]
  (fq-note instr t (mnote step) ms-length vol))

(defn song
  "Build a list of steps into a song"
  [instr ms-beat notes ms-lengths vols]
  (map vector (repeat instr) (range 0 1000000 ms-beat) notes ms-lengths vols))

(defn nilharp [x y z] (vector x y z))

(deftest fq-note-EXPECT-note
  (let [actual (fq-note nilharp 0 440 1000 0.5)]
  (is (= [440 1.0 0.5] actual))))

(deftest ms-beat-60-EXPECT-1000
  (let [actual (ms-beat 60)]
  (is (= 1000 actual))))

(deftest ms-beat-30-EXPECT-2000
  (let [actual (ms-beat 30)]
  (is (= 2000 actual))))

(deftest ms-beat-120-EXPECT-500
  (let [actual (ms-beat 120)]
  (is (= 500 actual))))

(deftest plink-EXPECT-note
  (let [actual (plink nilharp 1 2 1000 0.5)]
  (is (= [493.8833012561241 1.0 0.5] actual))))

(deftest song-EXPECT-list
  (let [actual (song nilharp 500 [0 1] (repeat 1000) (repeat 0.5))
        expected (list [nilharp 0 0 1000 0.5] [nilharp 500 1 1000 0.5])]
  (is (= expected actual))))

; harmonious sequence
(defn love
  "Harmonic frequencies to the love frequency 528"
  [x]
  (/ 528 x))

(defn harmonic
  "A harmonic frequency"
  [freq base-freq]
  (-> freq (+ 4) (* base-freq)))

(defn hnote
  [instr note of t ms-length vol]
  (let [fq (harmonic note (love of))]
    (fq-note instr t fq ms-length vol)))

(defn hplay
  [instr bpm offset base-div notes]
  (let [ms (ms-beat bpm)
        ms-length 1000
        vol 0.3]
    (map hnote
         (repeat instr)
         notes
         (repeat base-div)
         (range (* offset ms) 1000000 ms)
         (repeat ms-length)
         (repeat vol)))) 

; define some beats per minute
(def n 256)
(def bpm 120)
(def hab (/ bpm 2))
(def qub (/ bpm 4))
(def parb (* (/ bpm 4) 3))
(def trib (* (/ bpm 3) 1))
(def penb (* (/ bpm 5) 1))
(def sxb (* (* bpm 3) 1))

; this is the consistent length based on beats
(defn len [bbpm] (/ (* bbpm n) bpm))

; tests
(deftest love-2-EXPECT-264
  (let [actual (love 2)]
  (is (= 264 actual))))

(deftest harmonic-10-100-EXPECT-1400
  (let [actual (harmonic 10 100)]
  (is (= 1400 actual))))

(deftest hnote-EXPECT-notes
  (let [actual (hnote nilharp 4 8 10 1000 0.5)]
  (is (= [528 1.0 0.5] actual))))

(deftest hplay-EXPECT-play
  (let [actual (hplay nilharp 60 0 8 [0 1])]
  (is (= (list [264 1.0 0.1] [330 1.0 0.1]) actual))))

(run-tests 'ursound.har)
