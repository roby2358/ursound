(ns ursound.core
  (:use overtone.core)
  (:use ursound.server)
  (:use clojure.test)
  (:require [clojure.math.numeric-tower :as math]))

; some basic defines
(def a (math/expt 2 (/ 1 12)))
(defn mnote [hasteps] (* 440 (math/expt a hasteps)))

; kind of sounds like a piano
(definst pnano
  [freq 440 length 1 vol 0.4]
  (* vol
     (line:kr 1 0 length FREE)
     (saw freq)))

; tremulo instrument
(definst trem [freq 440 depth 10 vol 1.0 rate 6 length 3]
  (let [fq (+ freq (* depth (sin-osc:kr rate)))]
  (* vol
     (line:kr 0 0.1 length)
     (line:kr 1 0 length FREE)
     (saw [(- fq 10) (+ fq 10)]))))

; biaural oscillating
(definst oscc [freq 440 length 0.5 vol 0.8] 
  (out 0 (* vol
            (line:kr 0 0.8 0.05)
            (line:kr 1 0 (* length 0.6) FREE)
            (sin-osc [(- freq 10)  (+ freq 10)]))))

; saw wave example
(definst saw-wave [freq 440 attack 0.01 sustain 0.4 release 0.1 vol 0.4] 
  (* (env-gen (env-lin attack sustain release) 1 1 0 1 FREE)
     (saw freq)
     vol))

; spooky example
(definst spooky-house [freq 440 width 0.2 
                       attack 0.3 sustain 4 release 0.3 
                       vol 0.4] 
  (* (env-gen (lin attack sustain release) 1 1 0 1 FREE)
     (sin-osc (+ freq (* 20 (lf-pulse:kr 0.5 0 width))))
     vol))

; synthesizer example
;(defsynth sin1 [freq 440]
;  (out 0 (* vol (pink-noise) (sin-osc [(+ freq 10) (- freq 10)]))))

; example sample
(def kick        (sample (freesound-path 2086)))
(def snare       (sample (freesound-path 26903)))
(def close-hihat (sample (freesound-path 802)))
(def open-hihat  (sample (freesound-path 26657)))
(def clap        (sample (freesound-path 48310)))
(def gshake      (sample (freesound-path 113625)))
(def water-drops (sample (freesound-path 50623)))
(def click       (sample (freesound-path 406)))
(def powerwords  (sample (freesound-path 8323)))
(def muslmstreet  (sample (freesound-path 3447)))
(def beatlooptribal  (sample (freesound-path 209560)))
(def indothum  (sample (freesound-path 21663)))
(def bigperc  (sample (freesound-path 180812)))



; loop and play
(defn looper [nome sound]
  (let [beat (nome)]
    (at (nome beat) (sound))
    (apply-at (nome (inc beat)) looper nome sound [])))
