(ns ursound.play
  (:use overtone.core)
  (:use ursound.core)
  (:use ursound.har))


; twinkle twinkle
(def twinkle [0 0 7 7 9 9 7 5 5 4 4 2 2 0 7 7 5 5 4 4 2 7 7 5 5 4 4 2 0 0 7 7 9 9 7 5 5 4 4 2 2 0])
(map #(apply plink %) (song oscc 500 twinkle (repeat 1000) (repeat 0.3)))
(looper (metronome 120) kick)
(looper (metronome 210) click)
(stop)

[
;   (hplay oscc b 0 16 (rand-seq (len b)))
;   (hplay oscc b 0 16 (rand-seq (len b)))
;   (hplay oscc sxb 0 16 (rand-seq (len sxb)))
;   (hplay oscc qub 1 4(rand-seq (len qub)))
;   (hplay pnano hab 1 8(rand-seq (len qub)))
;   (hplay oscc qub 0 4 (rand-seq (len qub)))
;   (hplay oscc parb 1 9 (rand-seq parb-len))
;   (hplay oscc parb 0 3 (rand-seq parb-len))
;   (hplay spooky-house qub 8 (rand-seq (len qub)))
   (hplay trem qub 0 4 (rand-seq (len qub)))
   (hplay trem qub 1 2 (rand-seq (len qub)))
;   (looper (metronome b) bigperc)
;   (looper (metronome hab) gshake)
;   (looper (metronome sxb) gshake)
;   (looper (metronome parb) gshake)
;   (beatlooptribal)
   ]
(stop)
