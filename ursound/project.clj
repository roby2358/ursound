(defproject ursound "0.1.0-SNAPSHOT"
  :description "Ursound Project"
  :url "http://localhost:8080"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/math.numeric-tower "0.0.4"]
                 [overtone "0.9.1"]])
